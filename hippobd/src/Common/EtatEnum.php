<?php


namespace App\Common;

abstract class EtatEnum
{
    const TYPE_USE = "Usé";
    const TYPE_BON_ETAT = "Bon état";
    const TYPE_NEUF = "Neuf";

    /** @var array user friendly named type */
    protected static $typeName = [
        self::TYPE_USE => 'Usé',
        self::TYPE_BON_ETAT => 'Bon état',
        self::TYPE_NEUF => 'Neuf',
    ];

    /**
     * @param string $typeShortName
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_USE,
            self::TYPE_BON_ETAT,
            self::TYPE_NEUF
        ];
    }
}
