<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Query\ListeActusHandler;
use App\Domain\Query\ListeActusQuery;
use Symfony\Component\HttpFoundationRequest;
use App\Entity\Actu;
use App\Form\ActuDeleteFormulaireType;
use App\Form\ActuEditFormulaireType;
use App\Form\Type;
use App\Form\ActuFormulaireType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ActuController extends AbstractController
{
    /**
     * @Route("/", name="Pageactu")
     */
    public function index(ListeActusHandler $handler)
    {
        $listeActus=$handler->handle(new ListeActusQuery());
        return $this->render('actu/index.html.twig', [
            'controller_name' => 'ActuController',
            'actus' => $listeActus,
        ]);
    }


    /**
     * @Route("/admin/actu", name="addActu")
     */
    public function ajouterActu(Request $request){
        $actu = new Actu();
        $form = $this->createForm(ActuFormulaireType::class,$actu);

        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            $actu=$form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($actu);
            $entityManager->flush();
            return $this->redirectToRoute('Pageactu');
        }
        return $this->render('actu/AdminActu.html.twig', [
            'actu'=>$actu,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/admin/actu/suppr/{id}", name="supprActu")
     */
    public function DeleteActu(Request $request, $id)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $actu=$entityManager->getRepository('App\Entity\Actu')->find($id);
        if (null==$actu){
            throw new NotFoundHttpException("L'actualité recherchée n'existe pas");
        }
        $form = $this->createForm(ActuDeleteFormulaireType::class,$actu);
        $form->handleRequest($request);
        if($request->isMethod('POST')&&$form->isValid()){
            $entityManager->remove($actu);
            $entityManager->flush();
            return $this->redirectToRoute('Pageactu');
        }
        return $this->render('actu/AdminDeleteActu.html.twig', [
            'actu'=>$actu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/actu/edit/{id}", name="editActu")
     */
    public function EditActu(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actu = $entityManager->getRepository('App\Entity\Actu')->find($id);
        if (null==$actu){
            throw new NotFoundHttpException("L'actualité recherchée n'existe pas");
        }
        $form = $this->createForm(ActuFormulaireType::class,$actu);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {
            // Inutile de persister ici, Doctrine connait déjà notre annonce
            $entityManager->flush();
            //$request->getSession()->getFlashBag()->add('notice', 'Actualité modifiée.');
            return $this->redirectToRoute('Pageactu');
        }
        return $this->render('actu/AdminEditActu.html.twig',[
            'actu' => $actu,
            'form'   => $form->createView(),
        ]);
    }
}
