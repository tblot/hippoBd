<?php

namespace App\Controller;

use App\Domain\Query\ListeActusHandler;
use App\Domain\Query\ListeActusQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ListeActusHandler $handler)
    {
        $repository1 = $this->getDoctrine()->getManager()->getRepository("App\Entity\Album");
        $listeActus=$handler->handle(new ListeActusQuery());
        $albums = $repository1->findAll();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'actus' => $listeActus,
            'albums' => $albums,
        ]);

    }

}
