<?php

namespace App\Controller;

use App\Domain\Query\ListeLibrairiesHandler;
use App\Entity\Album;
use App\Entity\Librairie;
use App\Form\AlbumDeleteFormulaireType;
use App\Repository\AlbumRepository;
use App\Form\AlbumFormulaireType;
use App\Form\Type;
use App\Form\ActuFormulaireType;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use GuzzleHttp\Client;


use Symfony\Component\HttpFoundation\File\UploadedFile;

use Vich\UploaderBundle\Templating\Helper\UploaderHelper;



class AlbumController extends AbstractController
{
    /**
     * @Route("/market", name="marketplace")
     */
    public function index(Request $request,ListeLibrairiesHandler $handler)
    {
        // On créé la librairie
        $librairie = new Librairie();

        // On ajoute le formulaire et ses éléments
        $form = $this->createFormBuilder($librairie)
            ->add('nom')
            ->add('libraire')
            ->add('adresse')
            ->add('telephone')
            ->add('Valider', SubmitType::class)
            ->getForm();

        // on gère la requête
        $form->handleRequest($request);
        // retourne les données si le formulaire est envoyé et valide
        if ($form->isSubmitted() && $form->isValid()) {
            $librairie = $form->getData();
            $data = $this->getDoctrine()->getManager();
            $data->persist($librairie);
            $data->flush();
            return $this->redirectToRoute('marketplace');

        }
        $repository = $this->getDoctrine()->getManager()->getRepository('App\Entity\Librairie');
        $repository1 = $this->getDoctrine()->getManager()->getRepository("App\Entity\Album");
        $listeLibrairies = $repository->findAll();
        $albums = $repository1->findAll();

        return $this->render('album/index.html.twig', array(
            'form' => $form->createView(),
            'librairie' => $librairie,
            'librairies'=>$listeLibrairies,
            'controller_name' => 'AlbumController',
            'albums' => $albums,
        ));
    }
    /**
     * @Route("/admin/market", name="addAlbum")
     */
    public function ajouterAlbum(Request $request){
        $album = new Album();
        $form = $this->createForm(AlbumFormulaireType::class,$album);

        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            $album=$form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($album);
            $entityManager->flush();
            return $this->redirectToRoute('marketplace');
        }
        return $this->render('album/AdminAlbum.html.twig', [
            'album'=>$album,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/admin/market/suppr/{id}", name="supprAlbum")
     */
    public function DeleteAlbum(Request $request, $id)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $album=$entityManager->getRepository('App\Entity\Album')->find($id);
        if (null==$album){
            throw new NotFoundHttpException("L'album recherchée n'existe pas");
        }
        //Création formulaire vide avec champ CSRF pour protéger suppression d'album et éviter problème bdd
        //$form=$this->get('form.factory')->create();
        $form = $this->createForm(AlbumDeleteFormulaireType::class,$album);
        $form->handleRequest($request);
        if($request->isMethod('POST')&&$form->isValid()){
            $entityManager->remove($album);
            $entityManager->flush();
            // $this->addFlash('success', 'Article Created! Knowledge is power!');
            return $this->redirectToRoute('marketplace');
        }
        return $this->render('album/AdminDeleteAlbum.html.twig', [
            'album'=>$album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/market/edit/{id}", name="editAlbum")
     */
    public function EditAlbum(Request $request, Album $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $album = $entityManager->getRepository('App\Entity\Album')->find($id);
        if (null==$album){
            throw new NotFoundHttpException("L'album recherché n'existe pas");
        }
        $form = $this->createForm(AlbumFormulaireType::class,$album);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isValid()) {
            // Inutile de persister ici, Doctrine connait déjà notre annonce
            $entityManager->flush();
            //$request->getSession()->getFlashBag()->add('notice', 'Album modifiée.');
            return $this->redirectToRoute('marketplace');
        }
        return $this->render('album/AdminEditAlbum.html.twig',[
            'album' => $album,
            'form'   => $form->createView(),
        ]);
    }
}
