<?php

namespace App\DataFixtures;
use App\Entity\Actu;
use App\Entity\User;
use App\Entity\Album;
use App\Entity\Librairie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;



class AppFixtures extends  Fixture
{
    
    

    public function load(ObjectManager $manager)
    {   
        

        // Création de admin
        $admin = new User();

        $admin->setUsername('admin');
        $admin->setName('admin');
        $admin->setFirstName('admin');
        $admin->setEmail('admin@hippobd.com');
        // le mot de passe : 123
        $admin->setPassword('$argon2id$v=19$m=65536,t=4,p=1$pi3zJgVnHjGwWSgfrhxa8g$uFCNRpoHZxj0j9VujzFUt86Zj/h2xKx/hBC3bwKn7ok');
        $admin->setAdress('IUTLR');
        $admin->setPostalCode(17000);
        $admin->setCity('La Rochelle');
        $admin->setRoles("ROLE_ADMIN");

        $manager->persist($admin);
        $manager->flush();


       
        $titre = array('Scientifiction !','Festival international de la bande dessiné d\'Angoulême','Ouverture d\'une librairie Milles Sabords à Roubaix','"Les FDP de Tubonia", prolongations de la campagne !!!','Birds of Prey au cinéma','La nouvelle aventure d\'Asterix vient de sortir','Scrum Life, l\'adaptation en bandes dessinés arrive !!!','Le tome 18 de One Punch Man est enfin sortie','Les Indes fourbes, La BD grandemment recompensés','Olivia et les 6 autres nains');
        $description = array('Blake et Mortimer au musée des arts et métiers, profitez encore l\'exposition jusqu\'au 5 Janvier 2020','Venez faire un tour à la 47ème édition du festival du 30 Janvier au 2 Février 2020','Venez profitez de leur énorme catalogue','Prolongations de la campagne pour l\'achat du tome 2 des "FDP de Tubonia",Profitez en vous avez jusqu\'au 13/12','Retrouvez l\'adaptation du comics au cinéma, le 19 février 2020','Asterix et la fille de Vercingetorix, le dernier volume des aventures d\'Asterix','Découvrez la bande dessinés Scrum Life, rejoignez Jean-Pierre Lambert dans ces aventures des méthodes Agile !','La suite des aventures du Chauve Capée !!!','Les Indes Fourbes vainqueur du Grand prix RTL de la BD 2019 ainsi que de la sélection prix BD Fnac France Inter 2020','La BD féerique desormais en ventes dans toutes les librairies de France et de Navarre');
        

        // actu
        // create 12 actu
        for ($i = 0; $i < 10; $i++) {
            $actu = new actu();
            $actu->setTitre($titre[$i]);
            $actu->setDescription($description[$i]);
            $manager->persist($actu);

        }


        $manager->flush();


        $TitreAlbum = array('Les profs','Schtroumpfs','Asterix');
        $Auteur = array('Erroc','Peyo','Jean-Yves Ferri');
        $Editeur = array('Bamboo Edition','Le Lombard','Hachette');
        $Synopsis = array('Approchez, Mesdames et Messieurs ! Venez découvrir les seuls vrais aventuriers modernes : LES PROFS !
        Suivez le prof d\'histoire débutant, le prof de gym survitaminé, la prof de français sexy, le prof de philo blasé et la prof d\'anglais peau de vache dans leur croisade contre l\'ignorance et le poil dans la main !','L\'ensemble des albums raconte la vie des Schtroumpfs dans leur village au cœur d\'une forêt imaginaire en Europe durant un Moyen Âge mythifié, se défendant face à Gargamel et son chat Azraël ou partant dans de grandes aventures. Toute la tribu vit dans des champignons aménagés en maisons, dans un petit village au cœur de la forêt. Les Schtroumpfs sont petits et bleus avec une queue. Ils sont vêtus d\'un bonnet et d\'un pantalon blancs, à l\'exception de leur chef, le Grand Schtroumpf, vêtu de rouge, ainsi que quelques autres Schtroumpfs, comme le Schtroumpf bricoleur, le Schtroumpf paysan ou le Schtroumpf sauvage.','« Nous sommes en 50 avant Jésus-Christ. Toute la Gaule est occupée par les Romains... Toute ? Non ! Car un village peuplé d\'irréductibles Gaulois résiste à l\'envahisseur. Et la vie n\'est pas facile pour les garnisons de légionnaires romains des camps retranchés de Babaorum, Aquarium, Laudanum et Petibonum... »');
        $Genre = array('Bande dessinée jeunesse, Scolarité','Bande dessinée jeunesse, belge','Franco-belge, Humour, Aventure');
        $Vendeur = array('Marcel Uderzo','René Goscinny','Jean-Marc Morandini');
        $Etat = array('Neuf','Usé','Bon état');
        $Prix = array('8','6','7');
        $UpdatedAt = array(new \DateTime('06/04/2014'),new \DateTime('06/04/2014'),new \DateTime('06/04/2014'));
        $image =array('prof.jpg','Schtroumpfs.jpg','asterix.jpg');

        //album
        for ($i = 0; $i < 3; $i++) {


            $album = new album();
            $album->setTitre($TitreAlbum[$i]);
            $album->setAuteur($Auteur[$i]);
            $album->setEditeur($Editeur[$i]);
            $album->setSynopsis($Synopsis[$i]);
            $album->setGenre($Genre[$i]);
            $album->setVendeur($Vendeur[$i]);
            $album->setEtat($Etat[$i]);
            $album->setPrix($Prix[$i]);
            $album->setImage($image[$i]);
            $album->setUpdatedAt(new \DateTime('01/15/2020'));
            $album->setDateSortie(new \DateTime('06/04/2014'));
            

         

            
            $manager->persist($album);
        }


        $manager->flush();



        $Nom = array('Librairie Les Saisons','Librairie Calligrammes','Librairie Gréfine');
        $Libraire = array('Kathleen R. Burgoon','Terry B. Tavares','Serge Burns');
        $Adresse = array('160 rue de coureilles','13 rue de vaux de foletier','18 rue du petit soleil');
        $Telephone = array('0789545485','0666123890','0998877665');
        // librairie
        for ($i = 0; $i < 3; $i++) {


            $librairie = new librairie();
            $librairie->setNom($Nom[$i]);
            $librairie->setLibraire($Libraire[$i]);
            $librairie->setAdresse($Adresse[$i]);
            $librairie->setTelephone($Telephone[$i]);
    
            
            $manager->persist($librairie);
        }


        $manager->flush();
    }
}
