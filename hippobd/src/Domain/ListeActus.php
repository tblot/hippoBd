<?php


namespace App\Domain;

interface ListeActus
{
    public function toutesLesActus(): iterable;
}
