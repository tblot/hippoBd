<?php


namespace App\Domain;

interface ListeLibrairies
{
    public function toutesLesLibrairies(): iterable;
}