<?php
namespace App\Domain\Query;
use App\Domain\ListeActus;

class ListeActusHandler{
    private $actuListe;
    public function __construct(ListeActus $listeActu)
    {
        $this->actuListe=$listeActu;
    }
    public function handle(ListeActusQuery $requete):iterable
    {
        return $this->actuListe->toutesLesActus();
    }
    public function getActuListe()
    {
        return $this->actuListe;
    }

}