<?php


namespace App\Domain\Query;
use App\Domain\ListeLibrairies;


class ListeLibrairiesHandler
{
    private $LibrairiesListe;
    public function __construct(ListeLibrairies $listeLibrairies)
    {
        $this->LibrairiesListe=$listeLibrairies;
    }
    public function handle(ListeLibrairiesQuery $requete):iterable
    {
        return $this->LibrairiesListe->toutesLesLibrairies();
    }
    public function getLibrairiesListe()
    {
        return $this->LibrairiesListe;
    }
}