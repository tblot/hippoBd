<?php

namespace App\Form;

use App\Common\EtatEnum;
use App\Entity\Album;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class AlbumDeleteFormulaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class)
            ->add('auteur',TextType::class)
            ->add('editeur',TextType::class)
            ->add('date_sortie',DateType::class)
            ->add('synopsis',TextareaType::class)
            ->add('genre',TextType::class)
            ->add('vendeur',TextType::class)
            ->add('etat', ChoiceType::class, array(
                'required' => true,
                'choices' => EtatEnum::getAvailableTypes(),
                'choice_label' => function($choice) {
                    return EtatEnum::getTypeName($choice);
                },
            ))
            ->add('prix',MoneyType::class)
            ->add('image',TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
        ]);
    }
}
