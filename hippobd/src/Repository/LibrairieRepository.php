<?php

namespace App\Repository;

use App\Domain\ListeLibrairies;
use App\Entity\Librairie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Librairie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Librairie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Librairie[]    findAll()
 * @method Librairie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LibrairieRepository extends ServiceEntityRepository implements ListeLibrairies
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Librairie::class);
    }
    public function toutesLesLibrairies(): iterable
    {
        return $this->findAll();

    }
    // /**
    //  * @return Librairie[] Returns an array of Librairie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Librairie
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
