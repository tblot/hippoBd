<?php

// tests/Entity/ActuTest.php
namespace App\Tests\Entity;
use App\Entity\Actu;
use PHPUnit\Framework\TestCase;
class ActuTest extends TestCase
{
    public function testUri()
    {
        $actu = new actu();
        $actu->setTitre("title");
        $actu->setDescription("a good description");

        $this->assertEquals("title", $actu->getTitre());
        $this->assertEquals("a good description", $actu->getDescription());
    }

}