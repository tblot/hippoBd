<?php

// tests/Entity/AlbumTest.php
namespace App\Tests\Entity;
use App\Entity\Album;
use PHPUnit\Framework\TestCase;
class AlbumTest extends TestCase{


    public function testUri()
    {
        $album = new album();
        $album->setTitre("title");
        $album->setAuteur("Auteur");
        $album->setEditeur("editeur");
        $album->setSynopsis("Synopsis");
        $album->setGenre("genre");
        $album->setVendeur("vendeur");
        $album->setEtat("etat");
        $album->setPrix("20");



        $this->assertEquals("title", $album->getTitre());
        $this->assertEquals("Auteur", $album->getAuteur());
        $this->assertEquals("editeur", $album->getEditeur());
        $this->assertEquals("Synopsis", $album->getSynopsis());
        $this->assertEquals("genre", $album->getGenre());
        $this->assertEquals("vendeur", $album->getVendeur());
        $this->assertEquals("etat", $album->getEtat());
        $this->assertEquals("20", $album->getPrix());
    }

}