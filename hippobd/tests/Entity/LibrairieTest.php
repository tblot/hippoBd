<?php

// tests/Entity/LibrairieTest.php
namespace App\Tests\Entity;
use App\Entity\Librairie;
use PHPUnit\Framework\TestCase;
class LibrairieTest extends TestCase{


    public function testUri()
    {
        $librairie = new librairie();
        $librairie->setNom("librairie");
        $librairie->setLibraire("libraire");
        $librairie->setAdresse("adress");
        $librairie->setTelephone("0101010101");

        $this->assertEquals("librairie", $librairie->getNom());
        $this->assertEquals("libraire", $librairie->getLibraire());
        $this->assertEquals("adress", $librairie->getAdresse());
        $this->assertEquals("0101010101", $librairie->getTelephone());

    }
}