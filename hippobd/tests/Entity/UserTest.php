<?php

// tests/Entity/UserTest.php
namespace App\Tests\Entity;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
class UserTest extends TestCase{

    public function testUri()
    {
        $user = new user();
        $user->setUsername('username');
        $user->setName('name');
        $user->setFirstName('firstName');
        $user->setEmail('example@hippobd.com');
        $user->setPassword('password');
        $user->setAdress('adress');
        $user->setPostalCode(1000);
        $user->setCity('La Rochelle');
        $user->setRoles("ROLE_USER");

        
        $this->assertEquals("username", $user->getUsername());
        $this->assertEquals("name", $user->getName());
        $this->assertEquals("firstName", $user->getFirstName());
        $this->assertEquals("example@hippobd.com", $user->getEmail());
        $this->assertEquals("password", $user->getPassword());
        $this->assertEquals("adress", $user->getAdress());
        $this->assertEquals(1000, $user->getPostalCode());
        $this->assertEquals("La Rochelle", $user->getCity());


    }

}